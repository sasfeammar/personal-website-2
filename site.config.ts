export default {
  baseUrl:
    process.env.NEXT_PUBLIC_BASE_URL ||
    (process.env.NEXT_PUBLIC_VERCEL_URL &&
      `https://${process.env.NEXT_PUBLIC_VERCEL_URL}`) ||
    "https://uns.id/ammar",
  description:
    "Personal website of Ammar Al-Gifari. A passionate legal work and software engineer.",
  panelbear: process.env.NEXT_PUBLIC_PANELBEAR_ID as string,
};
