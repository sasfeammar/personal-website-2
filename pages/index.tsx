import Main from "@/components/layouts/Main";
import Image from "next/image";
import dynamic from "next/dynamic";
import Link from "@/components/Link";
import NextHead from "next/head";

const Doodle1 = dynamic(() => import("@/components/doodle/Doodle1"), {
  ssr: false,
});

const Doodle2 = dynamic(() => import("@/components/doodle/Doodle2"), {
  ssr: false,
});

export default function Index() {

  return (
    <Main className="justify-start md:justify-center">
      <NextHead>
        <meta
          name="keywords"
          content="ammar,amar,ammar sasfe,ahmad amar,ahmad amar al-gifari"
        />
      </NextHead>

      <div className="w-full flex flex-col-reverse md:flex-row justify-end md:justify-between items-center">
        <div className="flex flex-col items-center md:items-start space-y-4 justify-center">
          <div className="relative">
            <h1 className="noselect font-doodle mt-6 md:mt-1 text-3xl md:text-4xl">
              Hi, I&apos;m Ammar. 🐱
            </h1>
            <div className="absolute hidden lg:block z-[2] top-[-8px] right-6">
              <Doodle2 />
            </div>
          </div>
          <p className="noselect text-md md:text-lg text-center md:text-left max-w-[550px]">
          Hey there! I&apos;m a final-year law student with a passion for both law and IT.
          I love diving into everything related to law, science, & technology.
          Always exploring and learning something new!
          </p>
          <p className="text-center md:text-left max-w-[550px]">
            learn more{" "}
            <Link
              href="/about"
              className="underline relative z-10 font-semibold"
            >
              about me
            </Link>
            .
          </p>
        </div>
        <div className="relative w-[300px] md:w-[400px] noselect text-center">
          <Image alt="doodle" src="/me.png" priority width={300} height={300} />
          <div className="absolute z-10 bottom-[-80px] md:bottom-[-80px] left-[10px] md:left-[50px]">
            <Doodle1 />
          </div>
        </div>
      </div>
    </Main>
  );
}
