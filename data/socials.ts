export default [
  {
    name: "Email",
    slug: "ahmadamar.algifari@gmail.com",
    url: "mailto:ahmadamar.algifari@gmail.com",
  },
  {
    name: "Instagram",
    slug: "@ammarsasfe",
    url: "https://instagram.com/ammarsasfe",
  },
  {
    name: "LinkedIn",
    slug: "Ahmad Amar Al-Gifari",
    url: "https://www.linkedin.com/in/amaralgifari/",
  },
  {
    name: "Spotify",
    slug: "marss",
    url: "https://open.spotify.com/user/214e35xbnmpayfcnc3txrnq4y",
  },
];
