export type NavLink = {
  url: string;
  text: string;
};

export const links: NavLink[] = [
  {
    text: "About",
    url: "/about",
  },
];
