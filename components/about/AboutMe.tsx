import Link from "@/components/Link";
const AboutMe = () => {
  return (
    <section>
      <h2 id="about" className="scroll-margin-nav">
        <Link className="text-3xl" href="#about">
          About me
        </Link>
      </h2>
      <p>
        Hi, my name is <b>Ammar Al-Gifari</b>, you can call me{" "}
        <b>Ammar</b>.
      </p>
      <p>
        Final-year law student at Universitas Sebelas Maret with <b>2+ years of experience in work & internships</b>.
        Uniquely leveraging a distinctive combination of <b>law</b> and <b>IT capabilities</b> to enrich experiences.
        Possesses strong skills in analysis, agile methodologies, communication, and leadership.
      </p>
      <p>
        I am currently working as a <b>Corporate Secretary Intern</b> for <b>PT. XL Axiata, Tbk</b> in Jakarta, Indonesia.
      </p>
    </section>
  );
};

export default AboutMe;
