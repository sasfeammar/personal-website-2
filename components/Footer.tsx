const Footer = () => {
  return (
    <div className="noselect text-center text-sm font-doodle p-3 pb-20 md:pb-3">
      <span className="font-sans">©</span>
      2023 Wisesa & Crafted by Ammar.
    </div>
  );
};

export default Footer;
